git add includes/ misc/ modules/ profiles/ scripts/ themes/
git add CHANGELOG.txt
git checkout .gitignore

#optional
git checkout docker-compose*
git checkout settings.docker.php
git checkout wodby.yml
