# Drupal maintenance scripts

These scripts are designed to make it easier to update drupal

# Scripts

1. cleanupfiles.sh - removes drupal files except sites
1. updatefiles.sh - copyies in new drupal files
1. commitfiles.sh - stages new files ready for committing
 
## cleanupfiles.sh

* argument: root path eg cleanupfiles.sh /var/www/html

## updatefiles.sh

* argument: root path and dristribution in gz format eg updatefiles.sh /var/www/html ~/downloads/distribution.gz

## commitfiles.sh

* notes: run in www-root
* argument: null