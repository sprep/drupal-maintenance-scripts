WWWROOT=$1
GZFULLFILE=$2
GZFILE=$(basename -- "$GZFULLFILE")
TARFILE="${GZFILE%.*}"
TARFOLDER="${TARFILE%.*}"

#echo $WWWROOT
#echo $GZFULLFILE
#echo $TARFILE
#echo $TARFOLDER
#exit

cp $GZFULLFILE /tmp
gunzip /tmp/$GZFILE
cd /tmp
tar xvf $TARFILE
cd $WWWROOT
cp -R /tmp/$TARFOLDER/* .
cp -f /tmp/$TARFOLDER/.* .
